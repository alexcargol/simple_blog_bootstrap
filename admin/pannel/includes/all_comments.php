<?php include '../../../config.php'; 

$data = query ('SELECT * 
      FROM article 
	  ORDER BY id DESC');
	  
	$maxPerPage = 2;
	$currentPage = 1;
if (isset($_GET['page'])){
    $currentPage = $_GET['page'];
}
	$nrOfPages = ceil(count($data)/$maxPerPage);
	$startIndex = ($currentPage-1)*$maxPerPage;
	$data = array_slice($data, $startIndex, $maxPerPage);
$position = 2;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="../../../css/style.css">
</head>
<body>
    <div class="container" id="wrapper">
        <div class="row">
            
            <div class="col-12"><img class="card-img" src="../../../img/5.jpg" height="300" alt="Card image"><div class="card-img-overlay">
                <h1>&nbsp;Photography Blog</h1>
				<h4>&nbsp;Please write here your tag-line</h4>
				</div>
				
            </div>
        </div>
        <?php include 'admin_header.php'; ?>

        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Data</li>
                </ol>
            </nav>
			</div>
			<div class="row">
            <div id="content" class="col-9" style="background: #F5FFFA; ">
            
				<?php include 'all_comments_list.php'; ?> 
              
			</div>
            <?php include 'admin_sidebar.php'; ?> 
        </div> 
       <?php //include 'footer.php'; ?>
    </div>
</body>
</html>