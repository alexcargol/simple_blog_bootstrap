<?php include 'config.php'; 
 $commented=0;
 if (isset($_GET['id'])) { $id=intval($_GET['id']); }
 if (isset($_SESSION['id'])) { $id=$_GET['id']; }
  if (isset($_GET['commented'])) { $commented=intval($_GET['commented']); }
  
$data = query ('SELECT * 
      FROM article 
	  WHERE id='.$_GET['id'].'');
	  
$article = $data[0];
$title = $article['title'];
$author = $article['author'];
$content = $article['content'];
$date = $article['date']; 	
$picture = $article['picture'];

$query_comments = query ('SELECT * 
      FROM comments
	  WHERE art_id='.$_GET['id'].' AND approved = 1');
	
$_SESSION['id']=$id;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <div class="container" id="wrapper">
        <div class="row">
            
            <div class="col-12"><img class="card-img" src="img/5.jpg" height="300" alt="Card image"><div class="card-img-overlay">
                <h1>&nbsp;Photography Blog</h1>
				<h4 style="color:white">&nbsp;Please write here your tag-line</h4>
				</div>
				
            </div>
        </div>
        <?php include 'header.php'; ?>
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Data</li>
                </ol>
            </nav>
			</div>
			<div class="row">
            <div id="art_content" class="col-12 col-lg-9">
            
				<?php foreach ($data as $line) { echo " <div class='row' >
													<div class='col-12'>";				
							echo "<div class='shadow p-3 mb-5 bg-white rounded ' id='header_sm'> by: $line[author]<BR>on: $line[date]</div>
					<div class='shadow p-3 mb-5 bg-white rounded justify-content-center' id='title'><h2>$line[title]</h2></div>
					<br><div style='clear:both'></div>
                    <img src='$line[picture]' class='float-left' />
                    $content
					
                </div>
            </div> 
				 ";
						} ?>
               <br>
			   <div class="card my-4">
				<h5 class="card-header">COMMENTS</h5>
				<div class="card-body">
					<?php foreach ($query_comments as $line){ 
    echo "<div>";
				   
				   echo "<div class='border 0.5px solid black' id='comment_box'>
						<div id='comm'><b>$line[date]&nbsp;|&nbsp;</b></div>
						<div id='comm'><b>$line[author]&nbsp;|&nbsp;</b><br></div>
						<div id='comm'><b>$line[email]&nbsp;|&nbsp;</b></div>
						<div>$line[content]</div>
					   </div>	"; 
		
    echo "</div><div style='clear:both'></div>";
} ?>
				</div>
				</div><hr>
				<?php if ($commented==1) { echo "<div><br><center><h5>Thank you.<br/>Your comment has been submited for approval.</h5></center></div>"; 
				} else { echo"
				<form name='comments' method='post' action='post_comment.php?id=<?php echo $id; ?>'>
				 <div class='card my-4'>
				<h5 class='card-header'>POST YOUR COMMENT</h5>
				<div class='card-body'>
			<div class='form-group'>
				
					<label>Full Name</label>
					<input name='cauthor' type='text' class='form-control' id='cname' placeholder='Full Name'></label>
					<label for='exampleFormControlInput1'>Email address</label>
					<input type='text' name='cemail' class='form-control' id='exampleFormControlInput1' placeholder='name@example.com'>
			</div>
			<div class='form-group'>
				<label for='exampleFormControlTextarea1'>Comment</label>
				<textarea name='ccontent' class='form-control' id='exampleFormControlTextarea1' rows='3'></textarea>
			</div>
			<button type='submit' class='btn btn-primary'>Submit</button></div></div>
				</form> ";} ?>
			<br>
			</div>
            <?php include 'sidebar.php'; ?>
        </div> 
       <?php include 'footer.php'; ?>
    </div>
</body>
</html>